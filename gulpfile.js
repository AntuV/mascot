var gulp = require('gulp');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');

gulp.task('librerias', function() {
    // Compilar css de librerías y minificar
    gulp.src([
        'node_modules/owl.carousel/src/scss/owl.carousel.scss',
        'node_modules/owl.carousel/src/scss/owl.theme.default.scss'
        ]).pipe(rename({suffix: '.min'}))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest("css"));

    // Para que tenga un nombre identificativo y no "main"
    gulp.src(['node_modules/magnific-popup/src/css/main.scss'])
        .pipe(rename({basename: 'magnific-popup',suffix: '.min'}))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest("css"));

    // Mover archivos .min.js de librerías
    gulp.src([
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
        'node_modules/owl.carousel/dist/owl.carousel.min.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        'node_modules/sammy/lib/min/sammy-latest.min.js',
        'node_modules/sammy/lib/min/plugins/sammy.mustache-latest.min.js',
        'node_modules/mustache/mustache.min.js'
        ]).pipe(gulp.dest("lib"))
});

// Compilar y minificar estilos propios
gulp.task('estilos', function() {
    return gulp.src(['scss/*.scss'])
        .pipe(rename({suffix: '.min'}))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest("css"))
        .pipe(browserSync.stream());
});

// Servidor y watcher de archivos scss/html/js propios
gulp.task('serve', ['estilos'], function() {
    browserSync.init({
        server: "."  
    });
    gulp.watch(['scss/*.scss'], ['estilos']); //en themed.bootstrap.scss está bootstrap@4.1.0
    gulp.watch(['*.html', 'js/*.js', 'templates/*.html']).on('change', browserSync.reload);
});

gulp.task('default', ['librerias', 'serve']);
