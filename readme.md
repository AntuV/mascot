## Mascot

### Instrucciones

* `git clone https://bitbucket.org/AntuV/mascot`
* `cd mascot`
* Renombrar y editar archivo js/config.dist.js con la ruta a la API
* `npm install`
* `gulp`

El backend oficial se encuentra en [https://bitbucket.org/AntuV/ecommerce-backend/]
