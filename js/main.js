var stdParams = { json: true, cache: false };

var app = Sammy('#main', function() {
	this.use('Mustache','html');
	this.get('#/', function() {
		this.load(API_URL + 'inicio', stdParams)
			.render('templates/inicio.html')
			.swap();
	});
	this.get('#/producto/:id', function () {
		this.load(API_URL + 'producto/' + this.params['id'], stdParams)
	});
});

(function($) {
	$(function() {
		app.run('#/');

		// Hueso buscador
		$('#buscador').keydown(function(){ 
			var maxSize = 60;
			var minSize = 10; 
			var chars = $(this).val().length; 
			if (chars <= maxSize && chars >= minSize) {
				$(this).attr('size', chars);
			} else if (chars < minSize) {
				$(this).attr('size', minSize);
	        } else if (chars == 0) {
				$(this).attr('size', 30);
			}
	    }); 

		$('#buscador').focus(function () {
			$(this).attr('size', 10);
		});

		$('#buscador').blur(function () {
			if ($(this).val().length == 0) {
				$(this).attr('size', 30);
			}
		});
		// Fin Hueso Buscador
	});
})(jQuery);
